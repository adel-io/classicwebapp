# README #

Simple Single Page Application that uses Spring Boot as backend and ReactJs as Frontend.

### Backend Summary ###

* Spring Boot (Dependencies: Spring web (REST API),h2 database,Spring data JPA)
* h2 database that stores users using Java Persistence API (JPA)
* API users available at "http://localhost:8080/api/users"

###  Frontend Summary ###

* ReactJs
* Axios (Promise based HTTP client)
* React Bootstrap
* JSX syntax
