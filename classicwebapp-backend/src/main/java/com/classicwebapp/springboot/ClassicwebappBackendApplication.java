package com.classicwebapp.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.classicwebapp.springboot.model.User;
import com.classicwebapp.springboot.repository.UserRepository;

@SpringBootApplication
public class ClassicwebappBackendApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ClassicwebappBackendApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void run(String... args) throws Exception {
		userRepository.save(new User("George", "Boole", "George.Boole@gmail.com"));	
		userRepository.save(new User("Elon", "Musk", "Elon.Musk@gmail.com"));
		userRepository.save(new User("Mark", "Zuckerberg", "Mark.Zuckerberg@gmail.com"));
		userRepository.save(new User("Steve", "Jobs", "Steve.Jobs@gmail.com"));

	}

}
