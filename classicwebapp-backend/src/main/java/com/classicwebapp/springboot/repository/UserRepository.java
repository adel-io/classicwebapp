package com.classicwebapp.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classicwebapp.springboot.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
